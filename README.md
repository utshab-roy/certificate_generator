## Certificate Generator

This is an application that can read an excel file from the user and can generate certificate using the information form the excel file. The generated certificate will be in PDF format so that it would be very easy for the user to print out the desired certificate in the respective layout/design.


## Libraries  and Requirements

To implement this application we used two php libraries. One is for reading the excel file and another is to generate the output pdf file.For reading the excel file we used   **PHPExcel 1.8.1**(latest).  

The following software is required to develop using **PHPExcel:**

1.  PHP version 5.2.0 or newer
2.  PHP extension php_zip enabled
3.  PHP extension php_xml enabled
4.  PHP extension php_gd2 enabled



For generating the PDF file we used **DOMPDF 0.8.2** library.
The following software is required to develop using **DOMPDF:**

1.  PHP 5.4+ (required) 
2.  DOM extension (required, enabled by default) 
3.  GD extension 
4.  MBString extension. MBString is required for Unicode support.


## User guide
In the **data.xlsx** file the first column determines the name of the candidate, the second column represents the course that he/she completed and the last column represents the date of completion of the degree. We have included a demo data for easy understanding.  
The user just have to add data into to excel file and run the application. It will dynamically generate the certificate in the pdf folder.