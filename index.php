<?php
//include php libraries

//include phpexcel library
require_once 'libraries/phpexcel/PHPExcel.php';

//include dompdf library
require_once 'libraries/dompdf/autoload.inc.php';
use Dompdf\Dompdf;

//excel data file source: format: first col: Name, 2nd col: Course, 3rd col: Date
$tempfname = "data.xlsx"; //the local file name of the excel file

//read the excel file
$excelReader = PHPExcel_IOFactory::createReaderForFile($tempfname); // PHPExcel library to call the reader file
$excelObject = $excelReader->load($tempfname); //loading the excel file
$worksheet = $excelObject->getActiveSheet(); // activating the excel file for use
$lastRow = $worksheet->getHighestRow(); //getting the row number of the excel file
$lastCol = $worksheet->getHighestColumn(); //getting the column number of the file

//read each row in the data file/source file,
for ($i = 1; $i <= $lastRow; $i++){ //for loop start
    $name = $worksheet->getCell('A'.$i)->getValue(); // getting the cell value from from A column
    $course = $worksheet->getCell('B'.$i)->getValue(); // B column value
    $date = $worksheet->getCell('C'.$i)->getValue(); // C column value

    //let's build the html for certificate
    $html = '<html>
            <title>Certificate</title>
            <head>
            
                    
                <style>
                    body{
                        font-size: 16px;
                        line-height: 15px;
                        font-family: "DejaVu Sans";
                        
                      
                        background-image: url("assets/img/border.png");
                        background-repeat: no-repeat;
                        background-position: 50% -10%;
                        
                        padding: 150px;                                                                   
                    }    
                    .center{
                        text-align: center;
                        font-size: 20px;
                        padding: 10px;
                    }              
                
                    h1{
                        font-size: 25px;
                        line-height: 30px;
                        font-family: "DejaVu Sans";
                        text-align: center;
                        color: black;                                              
                    }
                    
                    .heading{                       
                        border-bottom: 4px solid black;
                        border-top: 4px solid black;
                        color: #00dd00;                      
                        margin-bottom: 35px;
                    }
                    
                    #date{
                        float: left;
                        padding-left: 80px;
                    }
                    
                    #sign{
                        float: right;
                        padding-right: 80px;
                    }

                    .inside{
                        margin-top: 50px;
                        text-align: center;  
                    }
                    
                </style>
            </head>
            <body>
                <div class="heading">
                    <h1>CERTIFICATE OF COMPLETION</h1>
                 <!--<img src="assets/img/logo.png" />-->
                </div>                      
                 
                            
                <div class="content">
                    <p class="center maintext">The Certificate is presented to '.$name.'</p>
                    <p class="center maintext">for successfully Completing the "'.$course.'</p>   
                    <div class="inside">
                        <p><span id="date">Date: '.$date.'</span><span id="sign">Sign _________</span></p>
                    </div>                          
                </div>
            </body>
        </html>

'; //end of html file

    // this block is used for the PDF file generator
    $dompdf = new Dompdf(); //creating dompdf object referrence
    $dompdf->loadHtml($html); // loading the html file through a php variable
    $dompdf->setPaper('A4', 'landscape'); // determining the page size. The page size and orientation can be changed as required

    $dompdf->render(); // rendering the html file to pdf file
    $output = $dompdf->output(); //this will create the output content

    //Create pdf folder if it doesn't already exist
    if (!file_exists('pdf')) {
        mkdir('pdf', 0777, true);
    }

    file_put_contents('pdf/certificate-'.$name.'.pdf', $output); // putting all the content inside the file and naming the file
}// for loop ends

echo 'PDF created successfully.'; //simple echo for confirmation the process
